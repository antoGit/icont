<?php

class Conexion{
    private $user;
    private $password;
    private $server;
    private $database;
    private $con;

    /**
     * @return mixed
     */
    public function __construct(){
        $user       = 'root';
        $password   = '';
        $server     = 'localhost';
        $database   = 'iconpos';

        $this->con  = new mysqli($server, $user, $password, $database);
    }

    public function getUser($usuario , $password){

        $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password ='" . $password . "'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()){
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getMenuMain(){

        $query = $this->con->query("SELECT * FROM `menu`");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()){
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getMenuMainVentas(){

        $query = $this->con->query("SELECT * FROM `menu` where acceso ='A'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()){
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }
//<!--Creando una nueva consulta  de usuarios, tabla usuarios-->
    public function getAllUserData(){

        $query = $this->con->query("SELECT * FROM usuarios");

        return $query;
    }

    //<!--Creando una nueva consulta-- -->
    public function getRegisterNewUser(){

        $query = $this->con->query("SELECT * FROM usuarios");

        return $query;
    }

    //borrando de la base de datos usario
    public function deleteUsuario($idUsuario){

        $query = $this->con->query("DELETE FROM usuarios WHERE id_usu=$idUsuario");

        return $query;
    }

    //editar o modificar usuario de la base
    public function updateUsuario($login,$tipo,$nombre,$foto,$password,$idUsuario){

        $query = $this->con->query("UPDATE `usuarios`
                                          SET `login` = '$login', `tipo` = '$tipo', 
                                              `nombre` = '$nombre', `password` = '$password', `foto` = '$foto' 
                                          WHERE `usuarios`.`id_usu` = $idUsuario");

        return $query;
    }
}

